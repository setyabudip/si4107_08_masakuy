package com.example.masakuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.masakuy.R;

public class splashscreen1 extends AppCompatActivity {

    private int waktu_loading=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                Intent home=new Intent(splashscreen1.this, com.example.masakuy.splashscreen2.class);
                startActivity(home);
                finish();

            }
        },waktu_loading);
    }
}
