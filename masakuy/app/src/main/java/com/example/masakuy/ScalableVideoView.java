package com.example.masakuy;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class ScalableVideoView extends VideoView {

    private int mVideoWidth;
    private int mVideoHeight;
    private DisplayMode displayMode = DisplayMode.ORIGINAL;

    public enum DisplayMode {
        ORIGINAL,
        FULL_SCREEN,
        ZOOM
    };

    public ScalableVideoView(Context context) {
        super(context);
    }

    public ScalableVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScalableVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(0, widthMeasureSpec);
        int height = getDefaultSize(mVideoHeight, heightMeasureSpec);

        if (displayMode == DisplayMode.ORIGINAL) {
            if (mVideoWidth > 0 && mVideoHeight > 0) {
                if ( mVideoWidth * height  > width * mVideoHeight ) {
                    height = width * mVideoHeight / mVideoWidth;
                } else if ( mVideoWidth * height  < width * mVideoHeight ) {
                    width = height * mVideoWidth / mVideoHeight;
                } else {

                }
            }
        }
        else if (displayMode == DisplayMode.FULL_SCREEN) {

        }
        else if (displayMode == DisplayMode.ZOOM) {

            if (mVideoWidth > 0 && mVideoHeight > 0 && mVideoWidth < width) {
                height = mVideoHeight * width / mVideoWidth;
            }
        }

        setMeasuredDimension(width, height);
    }

    public void changeVideoSize(int width, int height)
    {
        mVideoWidth = width;
        mVideoHeight = height;


        getHolder().setFixedSize(width, height);

        requestLayout();
        invalidate();
    }

    public void setDisplayMode(DisplayMode mode) {
        displayMode = mode;
    }
}
